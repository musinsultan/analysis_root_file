#include <iostream>
#include "fstream"
#include "array"
#include "vector"
#include "set"
#include "memory"
#include <algorithm>

#include "TFile.h"
#include "TTree.h"
#include "TH1F.h"
#include "TCanvas.h"
#include "TSystem.h"
#include "../root_include/include/EventInfo.hpp"
#include "../root_include/include/RunInfo.hpp"

using std::cout, std::endl, std::array, std::vector, std::string;

const string files_path = "../outputs/8/";
const int limit_runs_to_process = 0;

class CellHandler {
    array<int, 3> coordinate;
    double edep = 0;
    TH1F *h_spectrum = nullptr;
    TH1F *h_spectrum_after_veto = nullptr;


public:
    CellHandler(const array<int, 3> &coord) : coordinate(coord) {
        h_spectrum = new TH1F(GetName(), GetName(), 24000, 0, 1200);
        h_spectrum->GetXaxis()->SetTitle("Energy, keV");
        h_spectrum->SetLineColor(kBlue);
        h_spectrum->SetLineWidth(3);
    }

    void AddEdep(double e) {
        edep += e;
    }

    TString GetName() const {
        return Form("Cell_%d_%d_%d", coordinate[0], coordinate[1], coordinate[2]);
    }

    void FillEdep() {
        if (edep > 0.) h_spectrum->Fill(edep);
    }

    void FillEdepVeto() {
        if (!h_spectrum_after_veto) {
            h_spectrum_after_veto = new TH1F(GetName() + "_veto", GetName() + "_veto", 40, 0, 2);
            h_spectrum_after_veto->GetXaxis()->SetTitle("Energy, keV");
            h_spectrum_after_veto->SetLineColor(kBlue);
            h_spectrum_after_veto->SetLineWidth(3);
        }
        if (edep > 0.) h_spectrum_after_veto->Fill(edep);
    }

    void Reset() {
        edep = 0.;
    }

    int GetNumberOfEventsAfterVeto() {
        if (!h_spectrum_after_veto) return 0;
        return h_spectrum_after_veto->Integral(0, 20);
    }

    int GetNumberOfEventsBeforeVeto() {
        return h_spectrum->Integral(0, h_spectrum->FindBin(2.));
    }

    TH1F *GetHistogram() {
        return h_spectrum;
    }

    TH1F *GetHistogramAfterVeto() {
        return h_spectrum_after_veto;
    }

    void Draw() {
        auto c1 = new TCanvas();
        TH1F *h = (TH1F*) h_spectrum->Clone();
        h->Rebin(200);
        h->Draw();
        c1->SaveAs(Form("%s%s%s.png", files_path.c_str(), "pics/", GetName().Data()), "png");
        if (!h_spectrum_after_veto) return;
        h_spectrum_after_veto->Draw();
        c1->SaveAs(Form("%s%s%s%s", files_path.c_str(), "pics/", GetName().Data(), "_veto.png"), "png");
    }

};

class CellCollection {
    vector<array<int, 3>> coordinates = {};
    vector<CellHandler *> cells = {};
public:
    CellCollection() {}

    CellHandler *GetCell(const array<int, 3> &coord) {
        auto coord_find = std::find(coordinates.begin(), coordinates.end(), coord);
        if (coord_find != coordinates.end()) {
            return cells[std::distance(coordinates.begin(), coord_find)];
        } else {
            auto new_cell = new CellHandler(coord);
            cells.push_back(new_cell);
            coordinates.push_back(coord);
            return new_cell;
        }
    }

    void FillCells() {
        for (auto &cell: cells) {
            cell->FillEdep();
        }
    }

    void ResetCells() {
        for (auto &cell: cells) {
            cell->Reset();
        }
    }

    void DrawAllCells() {
        gSystem->Exec(Form("mkdir %spics", files_path.c_str()));
        for (auto &cell: cells) {
            cell->Draw();
        }
    }

};

class AnalysisEventObjects {
    vector<array<int, 3>> coordinates = {};
    vector<double> edeps = {};
    double min_time = std::numeric_limits<double>::max();
    double max_time = 0.;
    double veto_cut = 2.;

public:
    void AddEdep(array<int, 3> coord, double edep) {
        if (edep <= 0) return;
        auto coord_find = std::find(coordinates.begin(), coordinates.end(), coord);
        if (coord_find == coordinates.end()) {
            coordinates.push_back(coord);
            edeps.push_back(edep);
        } else {
            auto index = std::distance(coordinates.begin(), coord_find);
            edeps[index] += edep;
        }
    }

    void CalculateTimes(double time) {
        min_time = time < min_time ? time : min_time;
        max_time = time > max_time ? time : max_time;
    }

    double GetMaxTime() const {
        return max_time;
    }

    double GetMinTime() const {
        return min_time;
    }

    int GetNumberOfCells() const {
        return edeps.size();
    }

    void ApplyVeto(CellCollection *cells) {
        for (size_t i = 0; i < edeps.size(); i++) {
            if (edeps[i] > veto_cut) continue;
            if (edeps.size() > 1) continue;
            cells->GetCell(coordinates[i])->FillEdepVeto();
        }
    }
};

class AnalysisRunObjects {
    TH1F *time_diff_hist = nullptr;
    TH1F *init_cell_edep_hist = nullptr;
    int total_events = 0;
    int total_events_with_decay = 0;
    int total_events_with_edep = 0;

public:
    AnalysisRunObjects() {
        time_diff_hist = new TH1F("time_diff", "time_diff", 300, 0, 600);
        time_diff_hist->GetXaxis()->SetTitle("#Deltat, seconds");
        time_diff_hist->SetLineColor(kBlue);
        time_diff_hist->SetLineWidth(3);

        init_cell_edep_hist = new TH1F("init_cell_edep_hist", "init_cell_edep_hist", 12000, 0, 1200);
        init_cell_edep_hist->GetXaxis()->SetTitle("E_dep, keV");
        init_cell_edep_hist->SetLineColor(kBlue);
        init_cell_edep_hist->SetLineWidth(3);
    }

    void AddTotalNumberOfEvents(int n) {
        total_events += n;
    }

    void AddEventsWithDecay(int n) {
        total_events_with_decay += n;
    }

    void AddEventWithEdep() {
        total_events_with_edep += 1;
    }

    void AddEvent(AnalysisEventObjects *event) {
        time_diff_hist->Fill(event->GetMaxTime() - event->GetMinTime());
        if (event->GetNumberOfCells() > 0) {
            AddEventWithEdep();
        }
    }

    TH1F *GetTimeDiffHist() {
        return time_diff_hist;
    }

    TH1F *GetInitCellEdepHist() {
        return init_cell_edep_hist;
    }

    int GetNumberOfEventsWithEdep() {
        return total_events_with_edep;
    }

    int GetNumberOfEventsWithDecay() {
        return total_events_with_decay;
    }

    int GetTotalNumberOfEvents() {
        return total_events;
    }
};


int cluster() {
    std::ofstream output_txt(files_path+"output.txt", std::ofstream::trunc);

    // Create Cells Collection
    auto cells = new CellCollection();
    // Open input file
    auto input_file = TFile::Open((files_path + "g4output_pid_0.root").c_str());
    if (!input_file || input_file->IsZombie()) return 1;

    // Open output file
    auto output_file = TFile::Open((files_path + "analysis.root").c_str(), "RECREATE");

    // Analysis handlers
    auto analysis_run_objects = new AnalysisRunObjects();
    auto analysis_event_objects = std::make_unique<AnalysisEventObjects>();

    // Iterate over runs in one file
    int run_number = 0;
    while (true) {
        if (limit_runs_to_process > 0 && run_number >= limit_runs_to_process) break;
        // Check if run exists
        auto run_info_tree = input_file->Get<TTree>(Form("run_%d_run_info", run_number));
        auto event_info_tree = input_file->Get<TTree>(Form("run_%d_event_info", run_number));
        if (!run_info_tree || run_info_tree->IsZombie()) break;
        if (!event_info_tree || event_info_tree->IsZombie()) break;
        std::cout << "Starting run " << run_number << endl;

        // Set branch address
        RunInfo *run_info = new RunInfo;
        run_info_tree->SetBranchAddress(Form("b_run_%d_run_info", run_number), &run_info);
        run_info_tree->GetEntry(0);
        EventInfo *event_info = new EventInfo;
        event_info_tree->SetBranchAddress(Form("b_run_%d_event_info", run_number), &event_info);

        // Count events
        analysis_run_objects->AddTotalNumberOfEvents(run_info->events_in_run);
        analysis_run_objects->AddEventsWithDecay(event_info_tree->GetEntries());

        // Iterate over events in run
        for (int event = 0; event < event_info_tree->GetEntries(); event++) {
            event_info_tree->GetEntry(event);
            auto step_collection = event_info->GetStepCollection();

            analysis_event_objects.reset(new AnalysisEventObjects());
            // Iterate over steps in event
            for (const auto &step: step_collection) {
                auto x = step.GetCell_X_coordinate();
                auto y = step.GetCell_Y_coordinate();
                auto z = step.GetCell_Z_coordinate();
                auto coord = array<int, 3>({x, y, z});
                // Accumulate event information to run manager
                analysis_event_objects->CalculateTimes(step.GetTime());
                // Accumulate event information to global manager
                if (step.GetEdep() > 0) {
                    analysis_event_objects->AddEdep(coord, step.GetEdep());
                    cells->GetCell(coord)->AddEdep(step.GetEdep());
                }
            }
            // Send event to global manager
            analysis_run_objects->AddEvent(analysis_event_objects.get());
            analysis_event_objects->ApplyVeto(cells);
            cells->FillCells();
            cells->ResetCells();
        }
        run_number++;
        delete run_info;
        delete event_info;
    }

    auto run_info_tree = input_file->Get<TTree>("run_0_run_info");
    RunInfo *run_info = new RunInfo;
    run_info_tree->SetBranchAddress(Form("b_run_%d_run_info", run_number), &run_info);
    run_info_tree->GetEntry(0);
    auto dimensions = run_info->cube_dimensions;

    auto init_cell_coordinate = array<int, 3>(
            {(dimensions[0] - 1) / 2, (dimensions[1] - 1) / 2, (dimensions[2] - 1) / 2});
    auto init_cell = cells->GetCell(init_cell_coordinate);


    output_txt << "Before veto: " << init_cell->GetNumberOfEventsBeforeVeto() << endl;
    output_txt << "After veto: " << init_cell->GetNumberOfEventsAfterVeto() << endl;
    output_txt << "With edep: " << analysis_run_objects->GetNumberOfEventsWithEdep() << endl;
    output_txt << "With decay: " << analysis_run_objects->GetNumberOfEventsWithDecay() << endl;
    output_txt << "Total: " << analysis_run_objects->GetTotalNumberOfEvents() << endl;
    output_txt << double(init_cell->GetNumberOfEventsAfterVeto()) / analysis_run_objects->GetNumberOfEventsWithEdep() * 100
         << endl;
    output_txt << double(init_cell->GetNumberOfEventsAfterVeto()) / init_cell->GetNumberOfEventsBeforeVeto() * 100 << endl;

    cells->DrawAllCells();
    auto c1 = new TCanvas();
    auto h_init_cell = cells->GetCell(init_cell_coordinate)->GetHistogram();
    h_init_cell->Draw();
    h_init_cell->GetXaxis()->SetRangeUser(0, 2.);
    c1->SaveAs(Form("%s%s%s%s", files_path.c_str(), "pics/", h_init_cell->GetName(), "_resize.png"), "png");


    output_file->Write();
    output_file->Close();
//    input_file->Close();
    return 0;
}